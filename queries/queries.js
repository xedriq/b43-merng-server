const {ApolloServer, gql} = require("apollo-server-express")
const {GraphQLDateTime} = require('graphql-iso-date')

//bcrypt
const bcrypt = require('bcrypt')

// models
const Member = require('../models/Member')
const Task = require('../models/Task')
const Team = require('../models/Team')
const Item = require('../models/Item')

//nexmo
const Nexmo = require('nexmo');

// custom scalar
// resolver for Date Schema to be able to handle date data types
const customScalarResolver = {
	Date: GraphQLDateTime
}

const typeDefs = gql`
	# this is a comment
	# the type Query is the root of all GraphQL queries
	# this is use for executing GET requests


	scalar Date

	type TeamType {
		id: ID!
		name: String!
		tasks: [TaskType]
		createdAt: Date
		updateAt: Date
	}

	type TaskType {
		id:ID!
		description: String
		isCompleted: Boolean
		team: [TeamType]
		teamId: String
		createdAt: Date
		updateAt: Date
	}

	type MemberType {
		id: ID
		fname: String
		lname: String
		position: String
		team: TeamType
		teamId: String
		createdAt: Date
		updateAt: Date
		password: String!
		username: String!
	}

	type ItemType {
		id: ID
		name: String
		teamId: ID
		quantity: Int
		price: Float
		team: [TeamType]
		createdAt: Date
		updateAt: Date
	}

	#R in CRUD - retrieve functionality

	type Query {
		#create a query called hello that will expect a string data type

		hello: String!
		getTeams: [TeamType]
		getTeam(id:String! name:String): TeamType

		getTasks: [TaskType]
		getTask(id:ID): TaskType
		
		getMembers: [MemberType]
		getMember(id:String!): MemberType

		getItems: [ItemType]
		getItem(id:ID!): ItemType

	}

	#CUD in CRUD - Create Update Delete functionality

	type Mutation {
		createTeam(
			name: String!
			taskId: ID
		): TeamType
		
		createTask(
			description:String!
			teamId: String!
			isCompleted: Boolean
			teamId: String
		) : TaskType

		createMember(
			fname: String!
			lname: String!
			position: String!
			teamId: String
			password: String!
			username: String!
		) : MemberType

		createItem(
			name: String!
			quantity: Int
			teamId: ID!
			price: Float
		) : ItemType

		updateTeam(
			id: String!
			name: String
			tasks: [ID]
		): TeamType

		updateMember(
			id: String!
			fname: String
			lname: String
			position: String
			teamId: String
			password: String
			username: String
		):MemberType

		updateTask(
			id:ID!
			description: String!
			isCompleted: Boolean
			teamId: ID!
		):TaskType

		deleteTeam (
			id:String!
		): Boolean

		deleteMember (
			id:String!
		): Boolean

		deleteTask (
			id:String!
		): Boolean

		loginMember(username:String!, password: String!): MemberType

	}
`;

const resolvers = {
	// what are going to return/do when the queries are executed

	Query: {
		hello : ()=> null,
		getTeams: () => {
			return Team.find()
		},
		getTasks: ()=> {
			return Task.find()
		},
		getMembers: ()=>{
			return Member.find()
		},
		getMember: (obj, {id}, context, info) => {
			return Member.findById(id)
		},
		getTeam: (parent,{id})=>{
			return Team.findById(id)
		},
		getTask: (parent, {id})=>{
			// return Task.findById(args.id)
			return Task.findOne({_id:id})
		},
		getItems: ()=>{
			return Item.find()
		},
		getItem: (parent, {id})=>{
			return Item.findOne({_id:id})
		},

		
	},

	Mutation: {
		createTeam: (parent,{name, tasks})=>{
			 let newTeam = new Team({
			 	name,
			 	tasks
			 })

			return newTeam.save()
		},

		createTask: (parent, {description, teamId})=>{
			let newTask = new Task({
				description,
				teamId,
				isCompleted: false
			})

			return newTask.save()
		},

		createMember: (parent, {fname, lname, position, teamId, password, username}, context, info)=>{
			
			password = bcrypt.hashSync(password, 10)

			let newMember = new Member({
				fname,
				lname,
				position,
				teamId,
				password,
				username
			})

			return newMember.save()
		},

		createItem:(parent, {id, name, quantity, teamId, price})=>{
			let newItem = new Item({
				name,
				quantity,
				teamId,
				price
			})

			return newItem.save()
		},

		updateTeam: (parent, {id, name, tasks}, context, info)=>{
			return Team.findOneAndUpdate({_id:id}, {$set:{name, tasks}})
		},

		updateMember: (parent, {id, fname, lname, position, teamId, password, username}, context, info)=>{
			return Member.findOneAndUpdate({_id:id}, {$set:{fname, lname, position, teamId, password, username}})
		},

		updateTask: (parent, {id, description, isCompleted, teamId}, context, info)=>{
			return Task.findOneAndUpdate({_id:id}, {$set:{isCompleted, description}})
		},

		deleteTeam: (parent, {id}, context, info) => {
			return Team.findOneAndDelete({_id:id})

		},

		deleteTask: (parent, {id}, context, info) => {
			Task.findByIdAndDelete(id)
			.then(task=>{
				if(!task) {
					console.log('Task not found')
					return false
				}
				
				return true
			}).catch(err=>{
				console.log(err)
				return false
			})
		},

		deleteMember: (parent, {id}, context, info) => {
			
				Member.findByIdAndDelete(id)
				.then(member=>{
					if(!member){
						console.log("Member not found.")
						return false
					}
					console.log("Member Deleted")
					return true
				}).catch(err => {
					console.log(err.message)
					return false
				})
			
		},

		loginMember: (parent, args) => {
			// console.log(args)
			// returns the member with the matching args
			// return Member.findOne({fname: args.fname, password:args.password})
			return Member.findOne({username: args.username})
			.then(member=>{
				if(!member){
					console.log('No member found')
					return null
				}
				// console.log(member)

				// compare the hashed version of args.password with member.password(already hashed)
				let hashedPassword = bcrypt.compareSync(args.password, member.password)

				if(!hashedPassword){
					console.log('wrong password')
					return null
				} else {
					console.log(member)

					// const nexmo = new Nexmo({
					//   apiKey: 'c59e1314',
					//   apiSecret: 'O27lejJr9jQ5befY',
					// });

					// const from = 'Cedrick';
					// const to = '639266811463';
					// const text = 'You logged in!';

					// nexmo.message.sendSms(from, to, text);

					return member
				}
			}
		)}	
	},

	// custom object resolver
	// custom TeamType resolver
	TeamType: {
		// declare a resolver for the task field inside TeamType
		tasks: (parent, args)=>{
			return Task.find({teamId:parent._id})
		}
	},

	MemberType: {
		team: (parent, args)=>{
			return Team.findOne({_id:parent.teamId})
		}
	},

	TaskType :{
		team: (parent, args)=>{
			return Team.find({_id:parent.teamId})
		}
	},

	ItemType:{
		team: (parent, args)=>{
			return Team.find({_id:parent.teamId})
		}
	}

}


// create an instance of the apollo server
// In the most basic sence, the ApolloServer can be started by passing Schema type definitions(typeDefs) and the resolvers responsible for fetching the date for the declared requests/queries

const server = new ApolloServer({
	typeDefs,
	resolvers
})

module.exports = server