const mongoose = require('mongoose')
const Schema = mongoose.Schema // user mongoose's Schema as our basis

// Actual schema
const teamSchema = new Schema({
	name: {
		type: String,
		required: true
	}
}, {
	timestamps:true
})

// export the model as a module
module.exports = mongoose.model("Team", teamSchema);

