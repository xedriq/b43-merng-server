const mongoose = require('mongoose')
const Schema = mongoose.Schema // user mongoose's Schema as our basis

// Actual schema
const memberSchema = new Schema({
	fname: {
		type: String,
		required: true
	},
	lname: {
		type: String,
		required: true
	},
	position: {
		type: String,
		required: true
	},
	teamId: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required:true
	},
	username: {
		type: String,
		required: true,
		unique: true
	}
}, {
	timestamps:true
})

// export the model as a module
module.exports = mongoose.model("Member", memberSchema);

