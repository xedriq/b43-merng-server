const mongoose = require('mongoose')
const Schema = mongoose.Schema // user mongoose's Schema as our basis

// Actual schema
const itemSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},
	teamId: {
		type: String,
		required: true
	},
	quantity: {
		type: Number,
		required: true
	}
}, {
	timestamps:true
})

// export the model as a module
module.exports = mongoose.model("Item", itemSchema);

