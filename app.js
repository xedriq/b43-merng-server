// Declare dependencies
const express = require('express')
const app = express()
const mongoose = require('mongoose')
const port = 8000

// import models
const Team = require('./models/Team')
const Task = require('./models/Task')
const Member = require('./models/Member')
const Item = require('./models/Item')

// Database connection
// Local DB
// mongoose.connect("mongodb://localhost:27017/test_db_cedrick", {useNewUrlParser: true})

// Remote DB
mongoose.connect("mongodb+srv://xedriq:xedriq@cluster0-811so.mongodb.net/b43_merng_db?retryWrites=true&w=majority",
	{useUnifiedTopology: true, useFindAndModify : false})

mongoose.connection.once("open", ()=>{
	console.log('Connected to MongoDB Atlas...')
})

// use express' json body request handler
app.use(express.json())

// route
// app.get('/', function (req, res) {
//   res.send('<h1>Hello World</h1>')
// })

// app.get('/books', function (req, res) {
//   res.send('<h1>This is the Book Page</h1>')
// })


// let member_route = require('./routes/member_routes.js')
// let team_route = require('./routes/team_routes.js')

// app.use('/members', member_route)
// app.use('/teams', team_route)

//server initialization
// app.listen(port, ()=>{
// 	console.log('Server running at port ' + port)
// })


// import the instantiation of the apollo server

const server = require('./queries/queries')

// the app will be serve by the apollo server instead of express
server.applyMiddleware({
	app,
	// path:"/b43"
})

// server initialization
app.listen(port, ()=>{
	console.log(`🚀 Graphql server running on localhost:${port}${server.graphqlPath}`)
})