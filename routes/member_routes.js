const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')

const Member = require('../models/Member')


router.get("/", (req, res)=>{
	Member.find().then(members =>{
		res.send(members)
	})
})

// create a member
router.post("/", (req, res)=>{
	// res.send(`User ${req.body.lname}, ${req.body.fname} applying for ${req.body.position} position.`)
	let newMember = new Member({
		fname: req.body.fname,
		lname: req.body.lname,
		position: req.body.position
	})

	newMember.save((err, newMember)=>{
		// if an error was encountered while saving the document, output the error in the console
		if(err) return console.log(err)

		// if successful, return a response saying "Member created."
		res.send("Member created.")
	})
})

router.get('/:id',(req, res)=>{
	member = Member.findById(req.params.id, (err, member)=>{
		if(err) return console.log(err)
		res.send(member)
	})
})

router.put("/:id", (req, res)=>{
	Member.findOneAndUpdate({_id:req.params.id}, {
		$set:{
			fname: req.body.fname,
			lname: req.body.lname,
			position: req.body.position
		}
	}, (err, member)=>{
		if(err) return console.log(err)
		if(!member) return res.send('no user found')
		res.send('user updated')
	})
})


router.delete('/:id', (req, res)=>{
	Member.findOneAndDelete({_id : req.params.id}, (err, member)=>{
		console.log(member)
		if(err) {
			res.send('No member found')
			return console.log(err)
		}

		res.send('Member deleted!')
	})
})

module.exports = router