const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')

const Team = require('../models/Team')


router.get("/", (req, res)=>{
	Team.find().then(teams =>{
		res.send(teams)
	})
})

// create a member
router.post("/", (req, res)=>{
	// res.send(`User ${req.body.lname}, ${req.body.fname} applying for ${req.body.position} position.`)
	let newTeam = new Team({
		name: req.body.name,
	})

	newTeam.save((err, newTeam)=>{
		// if an error was encountered while saving the document, output the error in the console
		if(err) return console.log(err)

		// if successful, return a response saying "Team created."
		res.send("Team created.")
	})
})

router.get('/:id', (req,res)=>{
	team = Team.findById(req.params.id, (err, team)=>{
		if(err) return console.log(err)
		res.send(team)
	})
})

router.delete('/:id', (req,res)=>{
	Team.findOneAndDelete({_id : req.params.id}, (err, team)=>{
		if(err) {
			res.send('No team found')
			return console.log(err)
		}

		res.send('Team deleted!')
	})
})

module.exports = router